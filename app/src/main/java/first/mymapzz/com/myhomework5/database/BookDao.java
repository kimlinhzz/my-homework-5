package first.mymapzz.com.myhomework5.database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import first.mymapzz.com.myhomework5.model.Book;

@Dao
public interface BookDao {

    @Query("SELECT * from tb_book")
    LiveData<List<Book>> getAllBooks();

    @Query("SELECT * from tb_book where id =:id ")
    LiveData<Book> getBook(int id);

    @Query("SELECT * from tb_book where name LIKE '%'|| :bookName  || '%'")
    LiveData<List<Book>> getBookByName(String bookName);

    @Insert
    void insertBook(Book... book);

    @Update
    void updateBook(Book book);

    @Delete
    void deleteBook(Book book);
}
