package first.mymapzz.com.myhomework5.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.myhomework5.R;
import first.mymapzz.com.myhomework5.interfaces.OnRecyclerItemClickListener;
import first.mymapzz.com.myhomework5.model.Book;
import first.mymapzz.com.myhomework5.model.Category;
import first.mymapzz.com.myhomework5.repository.CategoryRepository;
import first.mymapzz.com.myhomework5.view_model.DataViewModel;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    private OnRecyclerItemClickListener onRecyclerItemClickListener;

    private List<Book> books;
    private CategoryRepository categoryRepository;
    private Context context;

    public BookAdapter(Context context, OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.books = new ArrayList<>();
        this.context = context;
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
        categoryRepository = new CategoryRepository(context);
        notifyDataSetChanged();
    }

    public void setData(List<Book> newData) {
        books.clear();
        Log.d("ADAPTER!@#", "Changing ");
        books.addAll(newData);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.book_list_item, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        Book book = books.get(position);
        Category category = categoryRepository.getCategoryTask(book.getCateID());
        TextView textName = holder.textViewName;
        TextView textCategory = holder.textViewCategory;
        TextView textSize = holder.textViewSize;
        textName.setText(book.getName());
        textSize.setText(String.valueOf(book.getSize()));
        textCategory.setText(category.getName());

        if (book.getImgURI() != null) {
            Log.d("URI!@#", book.getImgURI());
            holder.setBlogImage(book.getImgURI());
        } else {
            holder.imageCover.setImageResource(R.drawable.ic_launcher_foreground);
        }
    }

    @Override
    public int getItemCount() {
        if (books != null) {
            return books.size();
        } else {
            return 0;
        }
    }

    class BookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textViewName;
        TextView textViewCategory;
        TextView textViewSize;
        ImageView imageCover, imageMore;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.text_title_name);
            textViewCategory = itemView.findViewById(R.id.text_category_name);
            textViewSize = itemView.findViewById(R.id.text_size_value);
            imageCover = itemView.findViewById(R.id.image_cover_big);
            imageMore = itemView.findViewById(R.id.img_more);
            imageMore.setOnClickListener(this);
        }

        public void setBlogImage(final String downloadUri) {
            Glide.with(itemView.getContext()).load(downloadUri).into(imageCover);
        }

        @Override
        public void onClick(View view) {
            Book book = books.get(getAdapterPosition());
            onRecyclerItemClickListener.onItemClick(view, getAdapterPosition(), book);
        }
    }

    public void setOnRecyclerItemClickListener(OnRecyclerItemClickListener onRecyclerItemClickListener) {
        this.onRecyclerItemClickListener = onRecyclerItemClickListener;
    }
}
