package first.mymapzz.com.myhomework5.view_model;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import first.mymapzz.com.myhomework5.model.Book;
import first.mymapzz.com.myhomework5.model.Category;
import first.mymapzz.com.myhomework5.repository.BookRepository;
import first.mymapzz.com.myhomework5.repository.CategoryRepository;

public class DataViewModel extends AndroidViewModel {
    private BookRepository bookRepository;


    private LiveData<List<Book>> listLiveBookData;

    public DataViewModel(@NonNull Application application) {
        super(application);
        bookRepository = new BookRepository(application);
        listLiveBookData = bookRepository.getBookAllTasks();
    }

    public LiveData<List<Book>> getListLiveBookData() {
        return listLiveBookData;
    }

    public LiveData<List<Book>> getListLiveBookDataByName(String bookName) {
        return bookRepository.getBookByNameTask(bookName);
    }

    public LiveData<Book> getBook(int id) {
        return bookRepository.getBookTask(id);
    }

    public void bookInsert(String name, double price, String imageUri, int size, int cateID) {
        Book book = new Book();
        book.setName(name);
        book.setPrice(price);
        book.setImgURI(imageUri);
        book.setSize(size);
        book.setCateID(cateID);
        bookRepository.bookInsertTask(book);
    }

    public void bookDelete(Book book) {
        bookRepository.bookDeleteTask(book);
    }

    public void bookUpdate(Book book) {
        bookRepository.bookUpdateTask(book);
    }


}
