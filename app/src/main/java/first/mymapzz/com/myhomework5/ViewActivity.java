package first.mymapzz.com.myhomework5;

import androidx.appcompat.app.AppCompatActivity;
import first.mymapzz.com.myhomework5.model.Category;
import first.mymapzz.com.myhomework5.repository.CategoryRepository;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class ViewActivity extends AppCompatActivity {

    TextView textTitle, textCategory, textPrice, textSize;
    CategoryRepository categoryRepository;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        registerView();
        categoryRepository = new CategoryRepository(this);
        Intent data = getIntent();
        if (data != null) {

            textTitle.setText(data.getStringExtra("book_title"));
            textPrice.setText(String.valueOf(data.getDoubleExtra("book_price", 0.0)));
            textSize.setText(String.valueOf(data.getIntExtra("book_size", 0)));
            int tempCateID = data.getIntExtra("book_cateID", 0);
            Category category = categoryRepository.getCategoryTask(tempCateID);
            textCategory.setText(category.getName());
            String tempImg = data.getStringExtra("book_img");

            if (tempImg != null) {
                Log.d("IMAGE!@#", tempImg);
                Uri imgUri = Uri.parse(tempImg);
                imageView.setImageURI(null);
                imageView.setImageURI(imgUri);
                Glide.with(this).load(imgUri).into(imageView);
            } else {
                Glide.with(this).load(tempImg).placeholder(R.drawable.ic_launcher_foreground).into(imageView);
            }
        }

    }

    private void registerView() {
        textTitle = findViewById(R.id.text_title_name);
        textSize = findViewById(R.id.text_size_value);
        textPrice = findViewById(R.id.text_price_value);
        textCategory = findViewById(R.id.text_category_name);
        imageView = findViewById(R.id.image_cover_big);
    }
}
