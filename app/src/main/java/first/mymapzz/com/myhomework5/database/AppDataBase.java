package first.mymapzz.com.myhomework5.database;

import android.content.Context;

import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import first.mymapzz.com.myhomework5.model.Book;
import first.mymapzz.com.myhomework5.model.Category;

@Database(entities = {Book.class, Category.class}, version = 2)
public abstract class AppDataBase extends RoomDatabase {
    public abstract BookDao bookDao();

    public abstract CategoryDao categoryDao();

    public static AppDataBase INSTANCE;
    public synchronized static AppDataBase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = getAppDatabase(context);
        }
        return INSTANCE;
    }

    public static AppDataBase getAppDatabase(final Context context) {

        return INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDataBase.class, "" +
                "MyBookDB2")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                getInstance(context).categoryDao().insertCategory(populateData());
                            }
                        });
                    }
                })
                .allowMainThreadQueries()
                .build();

    }

    public static Category[] populateData() {
        return new Category[]{
                new Category("History"),
                new Category("Romance"),
                new Category("Comedy"),
                new Category("Technology"),
                new Category("Program")
        };
    }

    public static void DestroyInstance() {
        INSTANCE = null;
    }


}
