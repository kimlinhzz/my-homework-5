package first.mymapzz.com.myhomework5.interfaces;

import android.view.View;

import first.mymapzz.com.myhomework5.model.Book;

public interface OnRecyclerItemClickListener {

    void onItemClick(View view , int position , Book book);
}
