package first.mymapzz.com.myhomework5.repository;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import androidx.lifecycle.LiveData;
import first.mymapzz.com.myhomework5.database.AppDataBase;
import first.mymapzz.com.myhomework5.model.Book;

public class BookRepository {

    AppDataBase appDataBase;

    public BookRepository(Context context) {
        appDataBase = AppDataBase.getAppDatabase(context);
    }

    @SuppressLint("StaticFieldLeak")
    public void bookUpdateTask(Book book) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDataBase.bookDao().updateBook(book);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public void bookDeleteTask(final Book book) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDataBase.bookDao().deleteBook(book);
                return null;
            }
        }.execute();
    }


    @SuppressLint("StaticFieldLeak")
    public void bookInsertTask(final Book book) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                appDataBase.bookDao().insertBook(book);
                try {
                    publishProgress();
                    Thread.sleep(5000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
                Log.d("WAIT!@#", "waiting data to insert");
            }
        }.execute();
    }

    public LiveData<List<Book>> getBookByNameTask(String bookName) {
        return appDataBase.bookDao().getBookByName(bookName);
    }

    public LiveData<Book> getBookTask(int id) {
        return appDataBase.bookDao().getBook(id);
    }

    public LiveData<List<Book>> getBookAllTasks() {
        return appDataBase.bookDao().getAllBooks();
    }

}
