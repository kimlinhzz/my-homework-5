package first.mymapzz.com.myhomework5.repository;

import android.content.Context;

import java.util.List;

import first.mymapzz.com.myhomework5.database.AppDataBase;
import first.mymapzz.com.myhomework5.model.Category;

public class CategoryRepository {

    AppDataBase appDataBase;

    public CategoryRepository(Context context) {
        appDataBase = AppDataBase.getInstance(context);
    }

    public  List<Category> getCategoryAllTasks() {
        return appDataBase.categoryDao().getCategoryAll();
    }

    public  Category getCategoryTask(int id) {
        return appDataBase.categoryDao().getCategoryById(id);
    }

}
