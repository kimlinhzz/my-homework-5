package first.mymapzz.com.myhomework5;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import first.mymapzz.com.myhomework5.adapter.BookAdapter;
import first.mymapzz.com.myhomework5.model.Book;
import first.mymapzz.com.myhomework5.model.Category;
import first.mymapzz.com.myhomework5.repository.CategoryRepository;
import first.mymapzz.com.myhomework5.view_model.DataViewModel;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class DataDailogFragment extends DialogFragment implements View.OnClickListener, EasyPermissions.PermissionCallbacks {

    private static final int REQUEST_CODE_FOR_GALLERY = 1211;
    private static final int REQUEST_CODE_FOR_PERMISSION_GALLERY = 1111;
    private ImageView imgCover;
    private EditText edTitle, edSize, edPrice;
    private Button btnCancel, btnOk;
    private Spinner spinner;
    private FloatingActionButton fabImage;
    private DataViewModel dataViewModel;
    private CategoryRepository categoryRepository;
    private String tilte, imageURI = null;
    private int size;
    private double price;
    private Book book;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.diaglog_layout, container, false);
    }

    public DataDailogFragment(DataViewModel dataViewModel, Book book) {
        this.dataViewModel = dataViewModel;
        this.book = book;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerView(view);
        categoryRepository = new CategoryRepository(getActivity());
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, categoryRepository.getCategoryAllTasks());
        spinner.setAdapter(arrayAdapter);
        Log.d("DATABOOK!@#", "DATA" + book);
        if (book != null) {
            edTitle.setText(book.getName());
            edPrice.setText(String.valueOf(book.getPrice()));
            edSize.setText(String.valueOf(book.getSize()));
            spinner.setSelection(book.getCateID() - 1);
            btnOk.setText("Update");
            Log.d("DATABOOK!@#", "DATA2" + book);
            Glide.with(this).load(book.getImgURI()).into(imgCover);
        }
    }

    private void registerView(View view) {
        fabImage = view.findViewById(R.id.fab_image);
        imgCover = view.findViewById(R.id.image_cover_big);
        btnCancel = view.findViewById(R.id.btn_cancel);
        btnOk = view.findViewById(R.id.btn_ok);
        spinner = view.findViewById(R.id.spinner_cate);
        edTitle = view.findViewById(R.id.ed_title);
        edSize = view.findViewById(R.id.ed_size);
        edPrice = view.findViewById(R.id.ed_price);
        fabImage.setOnClickListener(this);
        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    private void insertBook() {
        try {
            Category category = (Category) spinner.getSelectedItem();
            tilte = edTitle.getText().toString();
            price = Double.parseDouble(edPrice.getText().toString());
            size = Integer.parseInt(edSize.getText().toString());
            if (!tilte.isEmpty()) {
                dataViewModel.bookInsert(tilte, price, imageURI, size, category.getId());
                Log.d("DATABOOK!@#", "DATA1" + book);
                Toast.makeText(getActivity(), "Successfully Inserted!!", Toast.LENGTH_LONG).show();
                dismiss();
            } else {
                Toast.makeText(getActivity(), "Your input can not be null", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Sorry!!Size and Price Can not be null and number only", Toast.LENGTH_LONG).show();
        }
    }

    private void updateBook() {
        Book bookTemp = new Book();
        try {
            Category category = (Category) spinner.getSelectedItem();
            tilte = edTitle.getText().toString();
            price = Double.parseDouble(edPrice.getText().toString());
            size = Integer.parseInt(edSize.getText().toString());
            bookTemp.setId(book.getId());
            bookTemp.setName(tilte);
            bookTemp.setCateID(category.getId());
            bookTemp.setImgURI(imageURI);
            bookTemp.setSize(size);
            bookTemp.setPrice(price);
            if (!bookTemp.getName().isEmpty() && bookTemp.getSize() != 0 && bookTemp.getPrice() != 0.0f) {
                dataViewModel.bookUpdate(bookTemp);
                Log.d("DATABOOK!@#", "DATA1" + book);
                Toast.makeText(getActivity(), "Successfully Updated!!", Toast.LENGTH_LONG).show();

                dismiss();
            } else {
                Toast.makeText(getActivity(), "Your input can not be null", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Sorry!!Size and Price Can not be null and number only", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

        if (view == btnOk) {

            if (book != null) {
                updateBook();
            } else {
                insertBook();
            }
        }
        if (view == btnCancel) {

            dismiss();
        }
        if (view == fabImage) {
            openGallery();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @AfterPermissionGranted(REQUEST_CODE_FOR_PERMISSION_GALLERY)
    void openGallery() {
        String[] prms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (EasyPermissions.hasPermissions(getActivity(), prms)) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, REQUEST_CODE_FOR_GALLERY);
            Toast.makeText(getActivity(), "Good to go", Toast.LENGTH_SHORT).show();
        } else {
            EasyPermissions.requestPermissions(getActivity(), "We need this permission for accessing gallery", REQUEST_CODE_FOR_PERMISSION_GALLERY, prms);
            Toast.makeText(getActivity(), "Requesting", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_FOR_GALLERY) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContext().getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Log.d("MainActivity123", imageStream.toString());
                imgCover.setImageBitmap(selectedImage);
                imageURI = imageUri.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(REQUEST_CODE_FOR_PERMISSION_GALLERY, permissions, grantResults, getActivity());
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(getActivity(), perms)) {
            new AppSettingsDialog.Builder(getActivity()).build().show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        book = null;
        imgCover = null;
        dataViewModel = null;
    }
}
