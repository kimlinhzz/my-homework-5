package first.mymapzz.com.myhomework5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import first.mymapzz.com.myhomework5.adapter.BookAdapter;
import first.mymapzz.com.myhomework5.interfaces.OnRecyclerItemClickListener;
import first.mymapzz.com.myhomework5.model.Book;
import first.mymapzz.com.myhomework5.view_model.DataViewModel;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener, OnRecyclerItemClickListener, PopupMenu.OnMenuItemClickListener, TextWatcher {

    BottomNavigationView bottomNavigationView;
    RecyclerView recyclerView;
    BookAdapter bookAdapter;
    Button btnDonate;
    TextView textNotFound;
    EditText editSearch;
    List<Book> listLiveData;
    DataViewModel dataViewModel;
    Book bookTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.nav_bar);
        textNotFound = findViewById(R.id.text_not_found);
        btnDonate = findViewById(R.id.btn_donate);
        editSearch = findViewById(R.id.ed_search);
        dataViewModel = ViewModelProviders.of(this).get(DataViewModel.class);
        editSearch.addTextChangedListener(this);
        btnDonate.setOnClickListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        recyclerView = findViewById(R.id.rcy_book);
        bookAdapter = new BookAdapter(MainActivity.this, this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
        recyclerView.setAdapter(bookAdapter);
        dataViewModel.getListLiveBookData().observe(MainActivity.this, dataItems -> {
            Toast.makeText(MainActivity.this, "Recreate", Toast.LENGTH_SHORT).show();
            setListData(dataItems);
            Log.d("DATA!@#", dataItems.toString());
        });
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
        return true;
    }

    public void setListData(List<Book> dataItemList) {
        //if data changed, set new list to adapter of recyclerview
        if (listLiveData == null) {
            listLiveData = new ArrayList<>();
        }
        listLiveData.clear();
        listLiveData.addAll(dataItemList);

        if (bookAdapter != null) {
            bookAdapter.setData(dataItemList);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnDonate) {
            DataDailogFragment dataDailogFragment = new DataDailogFragment(dataViewModel, null);
            dataDailogFragment.show(getSupportFragmentManager().beginTransaction(), "Hello");
            editSearch.setText("");
        }
    }


    @Override
    public void onItemClick(View view, int position, Book book) {
        if (book != null) {
            bookTemp = book;
        }
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.inflate(R.menu.option_more);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.view:
                viewData(bookTemp);
                return true;
            case R.id.edit:
                editSearch.setText("");
                DataDailogFragment dataDailogFragment = new DataDailogFragment(dataViewModel, bookTemp);
                dataDailogFragment.show(getSupportFragmentManager().beginTransaction(), "Hello");
                return true;
            case R.id.delete:
                deleteItem();
                return true;
        }
        return false;
    }

    void deleteItem() {
        Toast.makeText(MainActivity.this, "Click", Toast.LENGTH_SHORT).show();
        if (bookTemp != null) {
            new AlertDialog.Builder(this)
                    .setTitle("Delete")
                    .setMessage("Do you really want to delete this item?")
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dataViewModel.bookDelete(bookTemp);
                            Toast.makeText(MainActivity.this, "Item has been deleted", Toast.LENGTH_SHORT).show();
                        }
                    }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
                }
            }).show();
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }
    }

    void viewData(Book book) {
        Intent intent = new Intent(MainActivity.this, ViewActivity.class);
        if (book != null) {
            intent.putExtra("book_id", book.getId());
            intent.putExtra("book_title", book.getName());
            intent.putExtra("book_cateID", book.getCateID());
            intent.putExtra("book_size", book.getSize());
            intent.putExtra("book_price", book.getPrice());
            if (book.getImgURI() != null) {
                intent.putExtra("book_img", book.getImgURI());
            }
            startActivity(intent);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        Toast.makeText(this, "Changing..." + editable.toString(), Toast.LENGTH_SHORT).show();
        if (editable.toString().equals("") || editable.toString() == null || editable.toString().isEmpty() || editable.toString().length() == 0) {
            Toast.makeText(MainActivity.this, "edit text null" + editable.toString(), Toast.LENGTH_SHORT).show();
            dataViewModel.getListLiveBookData().observe(this, new Observer<List<Book>>() {
                @Override
                public void onChanged(List<Book> books) {
                    setListData(books);
                }
            });
        } else {
            dataViewModel.getListLiveBookDataByName(editable.toString()).observe(this, new Observer<List<Book>>() {
                @Override
                public void onChanged(List<Book> books) {
                    if (books != null) {
                        textNotFound.setVisibility(View.GONE);

                        setListData(books);
                    } else {
                        Toast.makeText(MainActivity.this, "No item is match with" + editable.toString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }
}

