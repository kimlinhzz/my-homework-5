package first.mymapzz.com.myhomework5.database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import first.mymapzz.com.myhomework5.model.Category;

@Dao
public interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(Category... category);

    @Query("Select * from tb_category")
    List<Category> getCategoryAll();

    @Query("Select * from tb_category where id = :cateId")
    Category getCategoryById(int cateId);
}
