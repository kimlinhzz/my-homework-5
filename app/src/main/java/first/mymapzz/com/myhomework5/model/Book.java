package first.mymapzz.com.myhomework5.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = "tb_book", foreignKeys = @ForeignKey(entity = Category.class,
        parentColumns = "id",
        childColumns = "cateID",
        onDelete = CASCADE))
public class Book {
    @PrimaryKey(autoGenerate = true)
    int id;
    String name;
    double price;
    String imgURI;
    int size;
    int cateID;

    public Book() {

    }

    public Book(String name, double price, String imgURI, int size, int cateID) {
        this.name = name;
        this.price = price;
        this.imgURI = imgURI;
        this.size = size;
        this.cateID = cateID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImgURI() {
        return imgURI;
    }

    public void setImgURI(String imgURI) {
        this.imgURI = imgURI;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getCateID() {
        return cateID;
    }

    public void setCateID(int cateID) {
        this.cateID = cateID;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", imgURI='" + imgURI + '\'' +
                ", size=" + size +
                ", cateID=" + cateID +
                '}';
    }


}
